
if(!isdefined(:SSNurbsTools))
    include("./ssnurbstoolsjl/nurbstools.jl")
end
if(!isdefined(:Kulfan))
    include("./kulfan.jl")
end
if(!isdefined(:SSNurbsFit))
    include("./ssnurbstoolsjl/nurbsfit.jl")
end

using SSNurbsFit
import SSNurbsFit.fit_b_spline

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.domain_X
import SSNurbsTools.bernstein_basis
import SSNurbsTools.del_bernstein_basis
import SSNurbsTools.outputObj

# Class Functions (help define Wing wing_geom)
function classF(x::Float64,
                n1::Float64,
                n2::Float64)
    return x.^n1.*(1-x).^n2
end
function classF_dfdx(x::Float64,
                     n1::Float64,
                     n2::Float64)
    if abs(x) < 1e-14
        return 0.0
    end
    result = n1 .* (1-x).^n2 .* x.^(n1-1) - n2 .* (1-x).^(n2-1) .* x.^n1
    return result
end
function wingF(x::Float64,
               xmin::Float64,
               xmax::Float64,
               n1::Float64,
               n2::Float64,
               a_coeffs::Array{Float64},
               alpha::Float64)
    #
    BernsteinS = 0
    p = length(a_coeffs)-1
    for i = 1:length(a_coeffs)
        BernsteinS += a_coeffs[i]*bernstein_basis(i,p,x;dmin=xmin,dmax=xmax)
    end
    classF_val = classF(x,n1,n2)
    result = classF_val * BernsteinS + alpha * x
    return result
end
function wingF_dfdx(x::Float64,
               xmin::Float64,
               xmax::Float64,
               n1::Float64,
               n2::Float64,
               a_coeffs::Array{Float64},
               alpha::Float64)
    #
    BernsteinS = 0
    p = length(a_coeffs)-1
    for i = 1:length(a_coeffs)
        BernsteinS += a_coeffs[i]*bernstein_basis(i,p,x;dmin=xmin,dmax=xmax)
    end

    BernsteinS_dfdx = 0
    p = length(a_coeffs)-1
    for i = 1:length(a_coeffs)
        BernsteinS_dfdx += a_coeffs[i]*del_bernstein_basis(i,p,x;dmin=xmin,dmax=xmax)
    end

    classF_val = classF(x,n1,n2)
    classF_dfdx_val = classF_dfdx(x,n1,n2)

    result = classF_val * BernsteinS_dfdx + classF_dfdx_val * BernsteinS + alpha
    return result
end

# Defining Wing Params
wing_L = 1.0
wing_depth = 1.5
class_n1 = 0.5
class_n2 = 1.0
del_xi_upper = 0.01

# Preparing the points that we want to fit the NURBS curve to.
xmin_up = 0.0
xmax_up = 1.0
a_coeffs_up = [0.2,0.2,0.2,0.2]
alpha_up = 0.
t_dir_up = -1

xmin_low = 0.0
xmax_low = 1.0
a_coeffs_low = [-0.05,-0.05,-0.05,-0.05]
alpha_low = 0.
t_dir_low = 1

nsamples = 500 #1000
degree = 3
n_bez = 50 #200


# Doing the fit
#f(x) = classF((xmax-x),class_n1,class_n2)
#dfdx(x) = classF_dfdx((xmax-x),class_n1,class_n2)

#=
f(x) = wingF(x,xmin_up, xmax_up, class_n1, class_n2, a_coeffs_up, alpha_up)
dfdx(x) = wingF_dfdx(x,xmin_up, xmax_up, class_n1, class_n2, a_coeffs_up, alpha_up)
t_dir = -1
wing_geom,WING_X = fit_b_spline(f,
                            dfdx,
                            xmin_up,
                            xmax_up,
                            t_dir_up,
                            nsamples,
                            degree,
                            n_bez)
=#


f_up(x) =         wingF(x,xmin_up, xmax_up, class_n1, class_n2, a_coeffs_up, alpha_up)
dfdx_up(x) = wingF_dfdx(x,xmin_up, xmax_up, class_n1, class_n2, a_coeffs_up, alpha_up)

f_low(x) =         wingF(x,xmin_low, xmax_low, class_n1, class_n2, a_coeffs_low, alpha_low)
dfdx_low(x) = wingF_dfdx(x,xmin_low, xmax_low, class_n1, class_n2, a_coeffs_low, alpha_low)

wing_geom,WING_X = fit_b_spline( [f_up,f_low],
                                [dfdx_up,dfdx_low],
                                [xmin_up,xmin_low],
                                [xmax_up,xmax_low],
                                [t_dir_up,t_dir_low],
                                nsamples,
                                degree,
                                n_bez)


@show wing_geom.knot_v


# **************************
# ********* Plotting *******
# **************************
using PyPlot; plt = PyPlot;

# Plotting the top of the wing
res = [1000]
CURVE_X,t_cur,h_cur = draw(wing_geom,res)
plt.figure()
plt.scatter(CURVE_X[1,:],CURVE_X[2,:],color="b")
plt.hold(true)
plt.scatter(wing_geom.control_pts[1,:],wing_geom.control_pts[2,:],color="r")
plt.plot(wing_geom.control_pts[1,:][:],wing_geom.control_pts[2,:][:],color="r")
# Plotting the exact-function of the top of the wing
plt.plot(WING_X[1,:][:],WING_X[2,:][:],color="g")
plt.hold(false)
plt.xlim([-0.2,1.2])
plt.ylim([-0.5,0.5])
plt.savefig("outputs/testfig2.pdf")




# Building the 2D wing
dim_p = 2
dim_s = 3
p = [wing_geom.degree[1],3]
knot_v = Array[wing_geom.knot_v[1],[0., 0, 0, 0, 1, 1, 1, 1]]
spans = [wing_geom.spans[1],3]
control_pts = zeros(dim_s,spans[1]+1,spans[2]+1)
for t1_i = 1:spans[1]+1
    for t2_i = 1:spans[2]+1
        control_pts[:,t1_i,t2_i] = Array{Float64}([wing_geom.control_pts[1,t1_i],
                                                   wing_geom.control_pts[2,t1_i],
                                                   (t2_i-1)*(wing_depth/spans[2])])
    end
end
weights = Array{Array{Float64}}(2)
weights[1] = wing_geom.weights[1]
weights[2] = ones(spans[2]+1)

wing_geom_3d = Nurbs(dim_p,
                     dim_s,
                     p,
                     knot_v,
                     spans,
                     control_pts,
                     weights)

res_to_draw = [1000,5]
X_3d,t_3d,h_3d = draw(wing_geom_3d,res_to_draw)
plt.figure()
plt.scatter3D(X_3d[1,:,:],X_3d[2,:,:],X_3d[3,:,:])
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("Surface")

outputObj(wing_geom_3d,"outputs/wing_obj.obj",res_to_draw)
