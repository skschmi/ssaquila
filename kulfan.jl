###############################################################################
# Filename: Kulfan.jl
# Description: A collection of functions for airfoil creation/parameterization
#              based on Kulfan parameters
# Author: Taylor McDonnell
# Email: taylor.golden.mcdonnell@gmail.com
# Date: 6 October 2016
# License: MIT License
###############################################################################

module Kulfan #Start Kulfan Module
import LsqFit
export CSTtoCoord,CoordtoCST


################################################################################
#--------------------------------Main Functions--------------------------------#
################################################################################


"""
function CSTtoCoord
=========

Uses Kulfan parameters to obtain airfoil coordinates

# Inputs:
*   Al: Lower surface Kulfan Parameters
*   Au: Upper surface Kulfan Parameters
*   ute: upper nondimensional trailing edge thickness ratio
*   lte: lower nondimensional trailing edge thickness ratio
*   N: number of points on airfoil for infinite trailing edge, finite trailing edge has N+1 points
*   N1,N2: Kulfan shape factors 0.5 and 1.0 respectively for airfoils
*   continuity in curvature around leading edge is achieved by Au[1] = -Al[1]

# Output:
*   x,y: x and y-coordinates of airfoil respectively
"""
function CSTtoCoord(;Al::Array{Float64,1}=[-1.0,-1.0,-1.0],
                     Au::Array{Float64,1}=[1.0,1.0,1.0],
                     ute::Float64=0.0,
                     lte::Float64=0.0,
                     N::Int64=250,
                     N1::Float64=0.5,
                     N2::Float64=1.0)


    # Initialize arrays
    x = zeros(N+1)
    y = zeros(N+1)
    zeta = zeros(N+1)

    # Use cosine spacing for x
    for i=1:N+1
        zeta[i] = 2*pi/(N)*(i-1)
        x[i] = 0.5*(cos(zeta[i])+1)
    end

    # Determine order of bernstein polynomials
    n = length(Au)-1

    # find leading edge
    lei = indmin(abs(x))
    # Split into upper and lower surfaces
    xl = x[1:lei]
    xu = x[(lei+1):end]
    # Use Kulfan Parameters to find corresponding y-coordinate
    y[1:lei]=KulfanParameterization(xl,N1,N2,Al,n,lte)
    y[(lei+1):end]=KulfanParameterization(xu,N1,N2,Au,n,ute)
    return x,y
end



"""
function CoordtoCST
==========

Uses airfoil coordinates to find Kulfan parameters

# Inputs
*   x: x-coordinates of airfoil
*   y: y-coordinates of airfoil
*   n: order of Kulfan Parameters

# Outputs:
*   Al,Au: Lower and upper kulfan parameters respectively
"""
function CoordtoCST(x,y,n=2,;N1=0.5,N2=1.0)
    # find upper and lower trailing edge thickness
    tei = find(x.==1.0)
    lte = minimum(y[tei])
    ute = maximum(y[tei])
    # remove all trailing edge points
    deleteat!(x,tei)
    deleteat!(y,tei)
    # add upper and lower trailing edge point back in
    unshift!(x,1)
    unshift!(y,lte)
    push!(x,1)
    push!(y,ute)
    # find number of points assuming infinite trailing edge
    N = length(x)
    # find leading edge
    lei = indmin(abs(x))
    # split points into upper and lower
    xl = x[1:lei]
    xu = x[(lei+1):end]
    yl = y[1:lei]
    yu = y[(lei+1):end]
    # initial guess of parameters
    Al = -ones(n+1)
    Au = ones(n+1)
    opt.N1 = N1
    opt.N2 = N2
    opt.n = n
    # perform optimization of lower surface
    opt.te = lte
    lfit = LsqFit.curve_fit(optModel,xl,yl,Al)
    # perform optimization of upper surface
    opt.te = ute
    ufit = LsqFit.curve_fit(optModel,xu,yu,Au)
    # lfit and ufit have additional information about quality of fit which is not
    # returned from this function
    return lfit.param,ufit.param
end


################################################################################
#------------------------------Helper Functions--------------------------------#
################################################################################


"""
function KulfanParameterization
==========

# Inputs:
*   x: nondimensional x coordinate
*   N1,N2: class function parameters
*   A: Kulfan Parameters
*   n: order of Kulfan Parameterization
*   te: nondimensional trailing edge location
"""
function KulfanParameterization(x::Array{Float64,1},
                                N1::Float64,
                                N2::Float64,
                                A::Array{Float64,1},
                                n::Int64,
                                te::Float64)
    return classFunction(x,N1,N2).*bernPoly(A,x,n)+x.*te
end



"""
function classFunction
==========

# Inputs:
*   x: nondimensional x coordinate
*   N1,N2: class function parameters
"""
function classFunction(x::Array{Float64,1},
                       N1::Float64,
                       N2::Float64)
    return x.^N1.*(1-x).^N2
end


"""
function bernPoly
=======

# Inputs:
*   A: kulfan parameters
*   x: nondimensional x coordinate
*   n: order of kulfan parameters
"""
function bernPoly(A::Array{Float64,1},
                  x::Array{Float64,1},
                  n::Int64)

    bp = zeros(length(x))
    for i  = 1:length(x)
        bp[i] = 0;
        for j = 1:(n+1)
            r = j-1
            K = factorial(n)/(factorial(r)*factorial(n-r))
            bp[i] += A[j]*K*(1-x[i])^(n-r)*x[i]^r
        end
    end
    return bp
end


"""
type optParam
=======
"""
type optParam
    N1::Float64
    N2::Float64
    n::Int64
    te::Float64
end


optParam() = optParam(0.5,1.0,3,0.0)
opt = optParam()

function optModel(x::Array{Float64,1},
                  p::Array{Float64,1})
    return KulfanParameterization(x,opt.N1,opt.N2,p,opt.n,opt.te)
end




end #End Kulfan Module
