



The commands to check out this repo, including the submodules:

git clone --recursive https://bitbucket.org/skschmi/ssaquila.git





If not all of the submodules are initialized, then try this:

git submodule update --init --recursive



To run:

julia -i wingbspline.jl 